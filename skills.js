
export const SKILLS = {
  // Disciple
  prayerOfHealing: {
    name: 'Prayer of Healing',
    xp: 0,
    cost: 1,
  },

  // Berserker
  rage: {
    name: 'Rage',
    xp: 0,
    cost: 1,
  }
}

export class Skill {
  constructor(desc){
    Object.assign(this, desc);
  }
}
