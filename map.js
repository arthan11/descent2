const EDGES = {
  transition: 'transition',
  wall: 'wall',
  door: 'door',
}

const SPACES_TYPES = {
  normal: 'normal',
  water: 'water',
  obstacle: 'obstacle',
  void: 'void',
}

export const TILES = {
  AEntrance: {
    name: 'Entrance A',
    width: 2,
    height: 2,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
    ]
  },
  AExit: {
    name: 'Exit A',
    width: 2,
    height: 2,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
    ]
  },
  ACorridor: {
    name: 'Corridor A',
    width: 2,
    height: 1,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
      {x: 1, y: 1, direction: 'bottom', type: EDGES.transition},
      {x: 2, y: 1, direction: 'bottom', type: EDGES.transition},
    ]
  },
  ADeadEnd: {
    name: 'Dead End A',
    width: 2,
    height: 1,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
    ]
  },
  A3: {
    name: '3A',
    width: 6,
    height: 4,
    exits: [
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
      {x: 3, y: 4, direction: 'bottom', type: EDGES.transition},
      {x: 4, y: 4, direction: 'bottom', type: EDGES.transition},
      {x: 6, y: 2, direction: 'right', type: EDGES.transition},
      {x: 6, y: 3, direction: 'right', type: EDGES.transition},
    ],
    spaces: [
      {x: 1, y: 1, type: SPACES_TYPES.water},
      {x: 2, y: 1, type: SPACES_TYPES.water},
      {x: 3, y: 1, type: SPACES_TYPES.water},
      {x: 4, y: 1, type: SPACES_TYPES.water},
      {x: 5, y: 1, type: SPACES_TYPES.water},
      {x: 6, y: 1, type: SPACES_TYPES.water},
    ]
  },
  A4: {
    name: '4A',
    width: 6,
    height: 6,
    exits: [
      {x: 3, y: 1, direction: 'top', type: EDGES.transition},
      {x: 4, y: 1, direction: 'top', type: EDGES.transition},
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
      {x: 1, y: 4, direction: 'left', type: EDGES.transition},
    ],
    spaces: [
      {x: 1, y: 1, type: SPACES_TYPES.obstacle},
      {x: 2, y: 2, type: SPACES_TYPES.obstacle},
      {x: 3, y: 3, type: SPACES_TYPES.obstacle},
      {x: 5, y: 6, type: SPACES_TYPES.obstacle},
      {x: 6, y: 6, type: SPACES_TYPES.obstacle},
      {x: 6, y: 5, type: SPACES_TYPES.obstacle},
    ]
  },
  A5: {
    name: '5A',
    width: 6,
    height: 4,
    exits: [
      {x: 3, y: 4, direction: 'bottom', type: EDGES.transition},
      {x: 4, y: 4, direction: 'bottom', type: EDGES.transition},
      {x: 6, y: 2, direction: 'right', type: EDGES.transition},
      {x: 6, y: 3, direction: 'right', type: EDGES.transition},
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
    ]
  },
  A6: {
    name: '6A',
    width: 4,
    height: 6,
    exits: [
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
      {x: 3, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 6, direction: 'bottom', type: EDGES.transition},
      {x: 3, y: 6, direction: 'bottom', type: EDGES.transition},
    ],
    spaces: [
      {x: 1, y: 3, type: SPACES_TYPES.water},
      {x: 2, y: 3, type: SPACES_TYPES.water},
      {x: 3, y: 3, type: SPACES_TYPES.water},
      {x: 4, y: 3, type: SPACES_TYPES.water},
    ]
  },
  A8: {
    name: '8A',
    width: 4,
    height: 4,
    exits: [
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
      {x: 3, y: 1, direction: 'top', type: EDGES.transition},
      {x: 4, y: 2, direction: 'right', type: EDGES.transition},
      {x: 4, y: 3, direction: 'right', type: EDGES.transition},
    ]
  },
  A9: {
    name: '9A',
    width: 4,
    height: 4,
    exits: [
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
      {x: 3, y: 1, direction: 'top', type: EDGES.transition},
      {x: 4, y: 2, direction: 'right', type: EDGES.transition},
      {x: 4, y: 3, direction: 'right', type: EDGES.transition},
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
    ]
  },
  A11: {
    name: '11A',
    width: 4,
    height: 4,
    exits: [
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
      {x: 4, y: 2, direction: 'right', type: EDGES.transition},
      {x: 4, y: 3, direction: 'right', type: EDGES.transition},
    ]
  },
  A12: {
    name: '12A',
    width: 5,
    height: 5,
    exits: [
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
      {x: 3, y: 5, direction: 'bottom', type: EDGES.transition},
      {x: 4, y: 5, direction: 'bottom', type: EDGES.transition},
    ],
    spaces: [
      {x: 1, y: 1, type: SPACES_TYPES.void},
      {x: 1, y: 4, type: SPACES_TYPES.void},
      {x: 1, y: 5, type: SPACES_TYPES.void},
      {x: 2, y: 4, type: SPACES_TYPES.void},
      {x: 2, y: 5, type: SPACES_TYPES.void},
      {x: 5, y: 5, type: SPACES_TYPES.void},
      {x: 2, y: 2, type: SPACES_TYPES.water},
      {x: 3, y: 2, type: SPACES_TYPES.water},
      {x: 4, y: 2, type: SPACES_TYPES.water},
      {x: 2, y: 3, type: SPACES_TYPES.water},
      {x: 3, y: 3, type: SPACES_TYPES.water},
      {x: 4, y: 3, type: SPACES_TYPES.water},
      {x: 3, y: 4, type: SPACES_TYPES.water},
      {x: 4, y: 4, type: SPACES_TYPES.water},
    ]
  },
  A18: {
    name: '18A',
    width: 4,
    height: 4,
    exits: [
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
      {x: 1, y: 4, direction: 'left', type: EDGES.transition},
      {x: 3, y: 1, direction: 'top', type: EDGES.transition},
      {x: 4, y: 1, direction: 'top', type: EDGES.transition},
    ],
    spaces: [
      {x: 1, y: 1, type: SPACES_TYPES.void},
      {x: 1, y: 2, type: SPACES_TYPES.void},
      {x: 2, y: 1, type: SPACES_TYPES.void},
      {x: 2, y: 2, type: SPACES_TYPES.void},
    ]
  },
  A21: {
    name: '21A',
    width: 6,
    height: 3,
    exits: [
      {x: 6, y: 1, direction: 'right', type: EDGES.transition},
      {x: 6, y: 2, direction: 'right', type: EDGES.transition},
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
    ],
    spaces: [
      {x: 2, y: 1, type: SPACES_TYPES.water},
      {x: 2, y: 2, type: SPACES_TYPES.water},
      {x: 4, y: 1, type: SPACES_TYPES.water},
      {x: 5, y: 1, type: SPACES_TYPES.water},
      {x: 4, y: 3, type: SPACES_TYPES.water},
    ]
  },

  BEntrance: {
    name: 'Entrance B',
    width: 2,
    height: 2,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
    ]
  },
  BExit: {
    name: 'Exit B',
    width: 2,
    height: 2,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
    ]
  },
  BCorridor: {
    name: 'Corridor B',
    width: 2,
    height: 1,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
      {x: 1, y: 1, direction: 'bottom', type: EDGES.transition},
      {x: 2, y: 1, direction: 'bottom', type: EDGES.transition},
    ]
  },
  BDeadEnd: {
    name: 'Dead End B',
    width: 2,
    height: 1,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
    ]
  },
  B3: {
    name: '3B',
    width: 6,
    height: 4,
    exits: [
      {x: 3, y: 4, direction: 'bottom', type: EDGES.transition},
      {x: 4, y: 4, direction: 'bottom', type: EDGES.transition},
      {x: 6, y: 2, direction: 'right', type: EDGES.transition},
      {x: 6, y: 3, direction: 'right', type: EDGES.transition},
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
      {x: 1, y: 3, direction: 'left', type: EDGES.transition},
    ]
  },
  B13: {
    name: '13B',
    width: 6,
    height: 6,
    exits: [
      {x: 3, y: 1, direction: 'top', type: EDGES.transition},
      {x: 4, y: 1, direction: 'top', type: EDGES.transition},
      {x: 5, y: 6, direction: 'bottom', type: EDGES.transition},
      {x: 6, y: 6, direction: 'bottom', type: EDGES.transition},
    ],
    spaces: [
      {x: 1, y: 1, type: SPACES_TYPES.void},
      {x: 6, y: 1, type: SPACES_TYPES.void},
      {x: 1, y: 5, type: SPACES_TYPES.void},
      {x: 2, y: 5, type: SPACES_TYPES.void},
      {x: 3, y: 5, type: SPACES_TYPES.void},
      {x: 4, y: 5, type: SPACES_TYPES.void},
      {x: 1, y: 6, type: SPACES_TYPES.void},
      {x: 2, y: 6, type: SPACES_TYPES.void},
      {x: 3, y: 6, type: SPACES_TYPES.void},
      {x: 4, y: 6, type: SPACES_TYPES.void},
    ]
  },
  B19: {
    name: '19B',
    width: 3,
    height: 6,
    exits: [
      {x: 1, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 1, direction: 'top', type: EDGES.transition},
      {x: 2, y: 6, direction: 'bottom', type: EDGES.transition},
      {x: 3, y: 6, direction: 'bottom', type: EDGES.transition},
    ]
  },
  B21: {
    name: '21B',
    width: 6,
    height: 3,
    exits: [
      {x: 6, y: 2, direction: 'right', type: EDGES.transition},
      {x: 6, y: 3, direction: 'right', type: EDGES.transition},
      {x: 1, y: 1, direction: 'left', type: EDGES.transition},
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
    ],
    spaces: [
      {x: 3, y: 1, type: SPACES_TYPES.water},
      {x: 3, y: 2, type: SPACES_TYPES.water},
      {x: 3, y: 3, type: SPACES_TYPES.water},
      {x: 4, y: 1, type: SPACES_TYPES.water},
      {x: 4, y: 2, type: SPACES_TYPES.water},
      {x: 4, y: 3, type: SPACES_TYPES.water},
    ]
  },
  B24: {
    name: '24B',
    width: 6,
    height: 2,
    exits: [
      {x: 1, y: 1, direction: 'left', type: EDGES.transition},
      {x: 1, y: 2, direction: 'left', type: EDGES.transition},
      {x: 5, y: 2, direction: 'bottom', type: EDGES.transition},
      {x: 6, y: 2, direction: 'bottom', type: EDGES.transition},
    ]
  },
}


class Space {
  constructor(tile, x, y) {
    this.tile = tile;
    this.x = x;
    this.y = y;
    this.edges = {
      top: EDGES.transition,
      right: EDGES.transition,
      bottom: EDGES.transition,
      left: EDGES.transition
    }
    this.monster = null;
    this.hero = null;
  }

  get isFree() {
    return (this.monster === null) && (this.hero === null);
  }

  get isObstacle() {
    return this.type === SPACES_TYPES.obstacle;
  }

  rotate() {
    const temp = this.edges.top;
    this.edges.top = this.edges.left;
    this.edges.left = this.edges.bottom;
    this.edges.bottom = this.edges.right;
    this.edges.right = temp;
  }
}

export class Tile {
  constructor(desc, caption=null) {
    this.spaces = [];
    this.name = desc.name;
    this.type = SPACES_TYPES.normal;
    this.caption = caption;
    for (let y = 0; y < desc.height; y++) {
      const line = [];
      for (let x = 0; x < desc.width; x++) {
        const space = new Space(this, x, y);
        if(y === 0) space.edges.top = EDGES.wall;
        if(y === desc.height-1) space.edges.bottom = EDGES.wall;
        if(x === 0) space.edges.left = EDGES.wall;
        if(x === desc.width-1) space.edges.right = EDGES.wall;
        line.push(space);
      }
      this.spaces.push(line);
    }

    if ('exits' in desc) {
      desc.exits.forEach(exit => {
        this.spaces[exit.y-1][exit.x-1].edges[exit.direction] = exit.type;
      })
    }

    if ('spaces' in desc) {
      desc.spaces.forEach(space_desc => {
        const spaceX = space_desc.x-1;
        const spaceY = space_desc.y-1;
        if(space_desc.type == SPACES_TYPES.void) {
          this.spaces[spaceY][spaceX] = null;

          if((spaceY !== desc.height-1) && (this.spaces[spaceY+1][spaceX].type !== SPACES_TYPES.void)) {
            this.spaces[spaceY+1][spaceX].edges.top = EDGES.wall;
          }
          
          if((spaceY !== 0) && (this.spaces[spaceY-1][spaceX] !== null)) {
            this.spaces[spaceY-1][spaceX].edges.bottom = EDGES.wall;
          } 

          if((spaceX !== desc.width-1) && (this.spaces[spaceY][spaceX+1].type !== SPACES_TYPES.void)) {
            this.spaces[spaceY][spaceX+1].edges.left = EDGES.wall;
          }

          if((spaceX !== 0) && (this.spaces[spaceY][spaceX-1] !== null)) {
            this.spaces[spaceY][spaceX-1].edges.right = EDGES.wall;
          } 

        } else {
          this.spaces[spaceY][spaceX].type = space_desc.type;
        }
      })
    }
  }

  rotate(times) {
    if(times > 0) {
      this.spaces = this.spaces[0].map((val, index) => this.spaces.map(row => row[index]).reverse())
      this.spaces.forEach( line => { line.forEach( space => { space?.rotate() }) });
      this.rotate(times-1);
    }
  }

}

export class Map {
  constructor(desc) {
    this.tiles = [];
    this.spaces = [];
    this.spaces = new Array(desc.height+2).fill(null).map( () => { return new Array(desc.width+2).fill(null) } );
    desc.tiles.forEach( tile_desc => {
      const tile = new Tile(tile_desc.nr, tile_desc.caption);
      tile.rotate(tile_desc.rotate);
      this.tiles.push(tile);
      this.putTileToMap(tile, tile_desc)
    })
  }

  putTileToMap(tile, tile_desc) {
    tile.spaces.forEach( (row, y) => {
      row.forEach((space, x) => {
        const spaceX = tile_desc.x + x;
        const spaceY = tile_desc.y + y;
        
        if (this.spaces[spaceY][spaceX] !== null) {
          throw `Space [${spaceX}x${spaceY}] already taken!`;
        }
        this.spaces[spaceY][spaceX] = space;
        if(space) {
          space.x = spaceX;
          space.y = spaceY;
        }

      })
    })
  }

  draw(targetElement) {
    const tileElement = document.createElement('div');
    tileElement.classList.add('tile');

    tileElement.style.setProperty("--size_x", this.spaces[0].length);
    tileElement.style.setProperty("--size_y", this.spaces.length);

    this.spaces.forEach( (line, y) => {
      line.forEach( (space, x) => {
        const spaceElement = document.createElement('div');
        spaceElement.classList.add('space');
        
        if(space === null) {
          spaceElement.classList.add('void');
        }
        else {
          // walls
          if(space.edges.top === EDGES.wall) spaceElement.classList.add('top-wall');
          if(space.edges.right === EDGES.wall) spaceElement.classList.add('right-wall');
          if(space.edges.bottom === EDGES.wall) spaceElement.classList.add('bottom-wall');
          if(space.edges.left === EDGES.wall) spaceElement.classList.add('left-wall');
          
          // doors
          if(space.edges.top === EDGES.door) spaceElement.classList.add('top-door');
          if(space.edges.right === EDGES.door) spaceElement.classList.add('right-door');
          if(space.edges.bottom === EDGES.door) spaceElement.classList.add('bottom-door');
          if(space.edges.left === EDGES.door) spaceElement.classList.add('left-door');
          
          // water
          if(space.type === SPACES_TYPES.water) spaceElement.classList.add('water');

          // obstacle
          if(space.type === SPACES_TYPES.obstacle) spaceElement.classList.add('obstacle');

          space.element = spaceElement;

          let title = space.tile.name;
          if(space.tile.caption !== null) {
            title += ' - ' + space.tile.caption;
          }
          spaceElement.setAttribute('title', title);
        }
        tileElement.append(spaceElement);
      })
    })

    targetElement.append(tileElement);
  }

  getTileByCaption(caption) {
    const tiles = this.tiles.filter( tile => {
      return tile.caption === caption;
    })
    return (tiles.length > 0) ? tiles[0] : null;
  }
}