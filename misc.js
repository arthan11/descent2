
export const RANGES = {
  malee: 'malee',
  ranged: 'ranged',
}

export const ITEMS_TRAITS = {
  blade: 'blade',
  shield: 'shield',
  axe: 'axe',
  hammer: 'Hammer',
}

export const MONSTERS_TRAITS = {
  building: 'building',
  cave: 'cave',
  wilderness: 'wilderness',
  dark: 'dark',
}