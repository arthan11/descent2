import { PLAYER_ROLES, Player } from './player.js';
import { Quest } from './quests.js';
import { Hero } from './heroes.js';
import { Item, SEARCH_ITEMS } from './items.js';

export class Game {
  constructor(){
    this.players = [];
    this.heroes = [];
    this.searchDeck = [];
    this.quest = null;
  }
 
  addPlayer(name) {
    const player = new Player(name);
    this.players.push(player);
    return player;
  }

  setOverlord(selectedPlayer) {
    this.players.forEach( player => {
      player.role = (player === selectedPlayer) ? PLAYER_ROLES.overlord : PLAYER_ROLES.heroes;  
    })
  }

  addHero(player, heroDesc, heroClassDesc) {
    if(player.role === PLAYER_ROLES.overlord) {
      throw `Can't add Hero to player: ${player.name}. This player has overlord role.`;
    }
    const hero = new Hero(heroDesc, heroClassDesc);
    this.heroes.push(hero);
    player.addHero(hero);
  }

  initSearchDeck(){
    this.searchDeck = [];
    Object.values(SEARCH_ITEMS).forEach( itemDesc => {
      for (let idx = 0; idx < itemDesc.amount; idx++) {
        const item = new Item(itemDesc);
        this.searchDeck.push(item);
      }
    })
  }

  selectQuest(questDesc) {
    this.quest = new Quest(questDesc, this.heroes);
  }

  putHeroesOnMap() {
    const tile = this.quest.map.getTileByCaption('Entrance');
    const spacesInTile = tile.spaces.flat();
    this.heroes.forEach(hero => {
      const spaces = spacesInTile.filter( space => space.isFree );
      const space = spaces[Math.floor(Math.random() * spaces.length)];
      space.hero = hero;
      hero.space = space;
    })
  }

  putMonsterOnSpaces(monster, spaces) {
    const baseSpace = spaces[Math.floor(Math.random() * spaces.length)];
    const monsterSize = (Math.floor(Math.random()*2) === 1) ? monster.size : [...monster.size].reverse();
    const monsterSpaces = [];

    for (let y = 0; y < monsterSize[1]; y++) {
      const line = [];
      for (let x = 0; x < monsterSize[0]; x++) {
        const posX = baseSpace.x + x;
        const posY = baseSpace.y + y;
        const space = this.quest.map.spaces[posY][posX];
        if((space !== null) && (baseSpace.tile === space.tile) && (space.isFree) && (!space.isObstacle)) {
          line.push(space);
        } else {
          this.putMonsterOnSpaces(monster, spaces);
          return;
        }
      }
      monsterSpaces.push(line);
    }
    
    monsterSpaces.forEach(line => {
      line.forEach(space => space.monster = monster);
    })
    monster.spaces = monsterSpaces;
  }

  putMonstersOnMap() {
    this.quest.monstersGroups.forEach(monstersGroup => {
      const tile = this.quest.map.getTileByCaption(monstersGroup.tileCaption);
      const spacesInTile = tile.spaces.flat();
      monstersGroup.monsters.forEach(monster => {
        monster.spaces = [];
        const spaces = spacesInTile.filter( space => {
          return (space.isFree) && (!space.isObstacle)
        })
        this.putMonsterOnSpaces(monster, spaces)
      })      
    })
  }
}