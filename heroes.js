import { DICES } from "./dices.js";
import { HeroClass } from './classes.js';
import { Item } from "./items.js";
import { Skill } from "./skills.js";

export const HEROES = {
  ashrian: {
    name: 'Ashrian',
    archetype: 'healer',
    speed: 5,
    health: 10,
    stamina: 4,
    defense: DICES.gray,
    might: 2,
    knowledge: 2,
    willpower: 3,
    awareness: 4
  },
  grisban_the_thirsty: {
    name: 'Grisban the Thirsty',
    archetype: 'warrior',
    speed: 3,
    health: 14,
    stamina: 4,
    defense: DICES.gray,
    might: 5,
    knowledge: 2,
    willpower: 3,
    awareness: 1
  },
  leoric_of_the_book: {
    name: 'Leoric of the Book',
    archetype: 'mage',
    speed: 3,
    health: 8,
    stamina: 5,
    defense: DICES.gray,
    might: 1,
    knowledge: 5,
    willpower: 2,
    awareness: 3
  },
  jain_fairwood: {
    name: 'Jain Fairwood',
    archetype: 'scout',
    speed: 5,
    health: 8,
    stamina: 5,
    defense: DICES.gray,
    might: 2,
    knowledge: 3,
    willpower: 2,
    awareness: 4
  },

}

export class Hero {
  constructor(desc, hero_class_desc) {
    if (!(desc.archetype === hero_class_desc.archetype)) {
      throw `Hero "${desc.name}" archetype (${desc.archetype}) is different from class archetype (${hero_class_desc.archetype})!`;
    }
    Object.assign(this, desc);
    this.class = new HeroClass(hero_class_desc);
    this.items = [];
    this.class.equipment.forEach(itemDesc => {
      const item = new Item(itemDesc);
      this.items.push(item);
    });
    this.skills = [];
    this.class.skills.forEach(skillDesc => {
      const skill = new Skill(skillDesc);
      this.skills.push(skill);
    });
    this.space = null;
  }

  draw() {
    this.element = document.createElement('div');
    this.element.setAttribute('title', this.name);
    this.element.classList.add('hero', this.archetype);
    this.space.element.append(this.element);
  }
}
