export const PLAYER_ROLES = {
  overlord: 'overlord',
  heroes: 'heroes'
}

export class Player {
  constructor(name) {
    this.name = name;
    this.role = null;
    this.heroes = [];
  }

  addHero(hero) {
    this.heroes.push(hero);
  }

}