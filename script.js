import { HEROES } from './heroes.js';
import { CLASSES } from './classes.js';
import { CAMPAIGNS } from './quests.js';
import { Game } from './game.js';

class Overlord {

}

const game = new Game();

const player1 = game.addPlayer('Player 1');
const player2 = game.addPlayer('Player 2');
const player3 = game.addPlayer('Player 3');

game.setOverlord(player3);


game.addHero(player1, HEROES.ashrian, CLASSES.disciple);
// game.addHero(player1, HEROES.leoric_of_the_book, CLASSES.runemaster);
game.addHero(player2, HEROES.grisban_the_thirsty, CLASSES.berserker);
// game.addHero(player2, HEROES.jain_fairwood, CLASSES.thief);

game.initSearchDeck();

game.selectQuest(CAMPAIGNS.HeirsofBlood.quests[0]);

game.putHeroesOnMap();
game.putMonstersOnMap();

const targetElement = document.querySelector('body');
game.quest.map.draw(targetElement);
game.heroes.forEach(hero => hero.draw());
game.quest.monstersGroups.forEach(monstersGroup => {
  monstersGroup.monsters.forEach(monster => {
    monster.draw();
  })
})

console.log(game);

// - Quest setup (objectives, search tokens, and villageras indicated on the quest map)
// - Create Overlord Deck (15 basic cards)
// - Draw Overlord Cards (equal to the number of heroes into hand)
