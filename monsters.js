import { DICES } from "./dices.js";
import { MONSTERS_TRAITS, RANGES } from "./misc.js";

export const MONSTER_GROUPS = {
  goblin_archer: {
    name: 'Goblin Archer',
    css_class: 'goblin-archer',
    attack_type: RANGES.ranged,
    traits: [MONSTERS_TRAITS.building, MONSTERS_TRAITS.cave],
    size: [1, 1],
    group_size: [
      {minions: 2, masters: 1},
      {minions: 3, masters: 1},
      {minions: 4, masters: 1},
    ],
    act_I: {
      minion: {
        speed: 5,
        health: 2,
        defense: [DICES.gray],
        abilities: [
          'Scamper',
          'Cowardly',
          'Surge: +1 Range',
          'Surge: +1 Heart'
        ],
        attack: [DICES.blue, DICES.yellow]
      },
      master: {
        speed: 5,
        health: 4,
        defense: [DICES.gray],
        abilities: [
          'Scamper',
          'Surge: +2 Range',
          'Surge: +2 Heart'
        ],
        attack: [DICES.blue, DICES.yellow]
      }
    },
    act_II: {
      minion: {
        speed: 5,
        health: 4,
        defense: [DICES.gray],
        abilities: [
          'Scamper',
          'Cowardly',
          'Surge: +2 Range',
          'Surge: +2 Heart'
        ],
        attack: [DICES.blue, DICES.yellow]
      },
      master: {
        speed: 5,
        health: 6,
        defense: [DICES.gray],
        abilities: [
          'Scamper',
          'Surge: +3 Range',
          'Surge: +2 Heart'
        ],
        attack: [DICES.blue, DICES.yellow]
      }
    }
  },
  barghest: {
    name: 'Barghest',
    css_class: 'barghest',
    attack_type: RANGES.malee,
    traits: [MONSTERS_TRAITS.wilderness, MONSTERS_TRAITS.dark],
    size: [1, 2],
    group_size: [
      {minions: 1, masters: 1},
      {minions: 2, masters: 1},
      {minions: 3, masters: 1},
    ],
    act_I: {
      minion: {
        speed: 4,
        health: 4,
        defense: [DICES.gray],
        abilities: [
          'Action: Howl',
          'Surge: +1 Heart',
        ],
        attack: [DICES.blue, DICES.red]
      },
      master: {
        speed: 4,
        health: 6,
        defense: [DICES.gray],
        abilities: [
          'Night Stalker',
          'Action: Howl',
          'Surge: +2 Heart'
        ],
        attack: [DICES.blue, DICES.red]
      }
    },
    act_II: {
      minion: {
        speed: 4,
        health: 6,
        defense: [DICES.black],
        abilities: [
          'Action: Howl',
          'Surge: +2 Heart',
        ],
        attack: [DICES.blue, DICES.red]
      },
      master: {
        speed: 4,
        health: 8,
        defense: [DICES.black],
        abilities: [
          'Night Stalker',
          'Action: Howl',
          'Surge: +2 Heart',
        ],
        attack: [DICES.blue, DICES.red, DICES.yellow]
      }
    }
  }
}

class Monster {
  constructor(name, variety, css_class, attack_type, traits, size, desc) {
    this.name = name;
    this.variety = variety;
    Object.assign(this, desc);
    this.spaces = [];
    this.css_class = css_class;
    this.attack_type = attack_type;
    this.traits = traits;
    this.size = size;
  }

  draw() {
    this.spaces.forEach((line, y) => {
      line.forEach((space, x) => {
        const element = document.createElement('div');
        element.setAttribute('title', `${this.name} [${this.variety}]`);
        element.classList.add('monster', this.css_class, this.variety);
        if((x==0) && (y==0)) element.classList.add('top-left');
        if((x==line.length-1) && (y==0)) element.classList.add('top-right');
        if((x==0) && (y==this.spaces.length-1)) element.classList.add('bottom-left');
        if((x==line.length-1) && (y==this.spaces.length-1)) element.classList.add('bottom-right');
        space.element.append(element);  
      })
    })
  }
}

export class MonsterGroup {
  constructor(desc) {
    this.desc = desc.group;
    this.tileCaption = desc.tileCaption;
    this.act = null;
    this.monsters = [];
  }

  create(heroes_number, act) {
    this.act = act;
    const group_size = this.desc.group_size[heroes_number-2];
    const desc_act = act === 2 ? this.desc.act_II : this.desc.act_I;
    
    // minions
    for (let monster_nr = 0; monster_nr < group_size.minions; monster_nr++) {
      const monster = new Monster(this.desc.name, 'minion', this.desc.css_class, this.desc.attack_type, this.desc.traits, this.desc.size, desc_act.minion);
      this.monsters.push(monster);  
    }
    // masters
    for (let monster_nr = 0; monster_nr < group_size.masters; monster_nr++) {
      const monster = new Monster(this.desc.name, 'master', this.desc.css_class, this.desc.attack_type, this.desc.traits, this.desc.size, desc_act.master);
      this.monsters.push(monster);  
    }
  }
}
