import { TILES, Map } from './map.js';
import { MONSTER_GROUPS, MonsterGroup } from './monsters.js';

export const CAMPAIGNS = {
  HeirsofBlood:
  {
    name: 'Heirs of Blood',
    quests: [
      {
        name: 'Acolyte of Saradyn',
        act: 1,
        map: {
          width: 11,
          height: 17,
          tiles: [
            {nr: TILES.ADeadEnd, rotate: 2, x: 3, y: 1},
            {nr: TILES.A5, rotate: 3, x: 2, y: 2, caption: 'Battlefield'},
            {nr: TILES.A21, rotate: 1, x: 3, y: 8},
            {nr: TILES.A9, rotate: 0, x: 3, y: 14, caption: 'Caravan Site'},
            {nr: TILES.A8, rotate: 3, x: 7, y: 14, caption: 'Firepit'},
            {nr: TILES.A11, rotate: 1, x: 7, y: 9},
            {nr: TILES.A4, rotate: 3, x: 6, y: 2, caption: 'Lair'},
            {nr: TILES.AEntrance, rotate: 1, x: 1, y: 15, caption: 'Entrance'},
            {nr: TILES.ACorridor, rotate: 0, x: 8, y: 8},
            {nr: TILES.ACorridor, rotate: 0, x: 8, y: 13},
          ]
        },
        monstersGroups: [
          {
            group: MONSTER_GROUPS.goblin_archer,
            tileCaption: 'Battlefield',
          },
          {
            group: MONSTER_GROUPS.barghest,
            tileCaption: 'Lair',
          },
        ]
      },
      {
        name: "Rellegar's Rest E1",
        act: 1,
        map: {
          width: 16,
          height: 13,
          tiles: [
            {nr: TILES.AEntrance, rotate: 0, x: 2, y: 12, caption: 'Entrance'},
            {nr: TILES.A6, rotate: 0, x: 1, y: 6, caption: 'Stream'},
            {nr: TILES.A18, rotate: 2, x: 2, y: 2},
            {nr: TILES.A3, rotate: 0, x: 6, y: 1, caption: "River's Edge"},
            {nr: TILES.A12, rotate: 0, x: 12, y: 1, caption: 'Pond'},
            {nr: TILES.ADeadEnd, rotate: 0, x: 14, y: 6},
            {nr: TILES.A21, rotate: 3, x: 8, y: 5},
            {nr: TILES.AExit, rotate: 0, x: 9, y: 11, caption: 'Exit'},
          ]
        },
        monstersGroups: [
          {
            group: MONSTER_GROUPS.goblin_archer,
            tileCaption: 'Stream',
          },
        ],
      },
      {
        name: "Rellegar's Rest E2",
        act: 1,
        map: {
          width: 7,
          height: 22,
          tiles: [
            {nr: TILES.BDeadEnd, rotate: 2, x: 3, y: 1},
            {nr: TILES.B13, rotate: 0, x: 1, y: 2, caption: 'Library'},
            {nr: TILES.B3, rotate: 1, x: 4, y: 8, caption: 'Armory'},
            {nr: TILES.B19, rotate: 2, x: 5, y: 14, caption: 'Stairway'},
            {nr: TILES.BEntrance, rotate: 0, x: 6, y: 20, caption: 'Entrance'},
            {nr: TILES.B24, rotate: 3, x: 2, y: 10},
            {nr: TILES.B21, rotate: 1, x: 1, y: 16},
            {nr: TILES.BDeadEnd, rotate: 0, x: 1, y: 22},
            
          ]
        },
        monstersGroups: [
          {
            group: MONSTER_GROUPS.goblin_archer,
            tileCaption: 'Stairway',
          },
        ],
      }
    ]
  }
}


export class Quest {
  constructor(desc, heroes) {
    this.heroes = heroes;
    this.name = desc.name;
    this.act = desc.act;
    this.map = new Map(desc.map);

    this.monstersGroups = [];
    desc.monstersGroups.forEach( group_desc => {
      const monsterGroup = new MonsterGroup(group_desc);
      monsterGroup.create(this.heroes.length, this.act);
      this.monstersGroups.push(monsterGroup);
    })
  }
}