import { CLASSES_ITEMS } from './items.js';
import { SKILLS } from './skills.js';

export const CLASSES = {
  // healers
  disciple: {
    name: 'disciple',
    archetype: 'healer',
    equipment: [CLASSES_ITEMS.ironMace, CLASSES_ITEMS.woodenShield],
    skills: [SKILLS.prayerOfHealing]
  },
  spiritspeaker: {
    name: 'spiritspeaker',
    archetype: 'healer',
    equipment: [],
    skills: []
  },

  // warriors
  knight: {
    name: 'knight',
    archetype: 'warrior',
    equipment: [CLASSES_ITEMS.ironLongsword, CLASSES_ITEMS.woodenShield],
    skills: []
  },
  berserker: {
    name: 'berserker',
    archetype: 'warrior',
    equipment: [CLASSES_ITEMS.chippedGreataxe],
    skills: [SKILLS.rage]
  },

  // mages  
  necromancer: {
    name: 'necromancer',
    archetype: 'mage',
    equipment: [],
    skills: []
  },
  runemaster: {
    name: 'runemaster',
    archetype: 'mage',
    equipment: [],
    skills: []
  },

  // scouts
  thief: {
    name: 'thief',
    archetype: 'scout',
    equipment: [],
    skills: []
  },
  wildlander: {
    name: 'wildlander',
    archetype: 'scout',
    equipment: [],
    skills: []
  },

}

export class HeroClass {
  constructor(desc) {
    Object.assign(this, desc);
  }
}