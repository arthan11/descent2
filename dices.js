export const DICES = {
  blue: 'blue',
  red: 'red',
  yellow: 'yellow',
  gray: 'gray',
  brown: 'brown',
  black: 'black'
}