import { DICES } from "./dices.js"
import { RANGES, ITEMS_TRAITS } from "./misc.js"

const WORN = {
  oneHand: 'One Hand',
  twoHands: 'Two Hands',
}

export const CLASSES_ITEMS = {
  ironLongsword: {
    name: 'Iron Longsword',
    attackDices: [DICES.blue, DICES.red],
    range: RANGES.malee,
    trait: ITEMS_TRAITS.blade,
    worn:	WORN.oneHand,
  },
  woodenShield: {
    name: 'Wooden Shield',
    trait: ITEMS_TRAITS.shield,
    worn:	WORN.oneHand,
  },
  chippedGreataxe: {
    name: 'Chipped Greataxe',
    attackDices: [DICES.blue, DICES.red],
    range: RANGES.malee,
    trait: ITEMS_TRAITS.axe,
    worn:	WORN.twoHands,
  },
  ironMace: {
    name: 'Iron Mace',
    attackDices: [DICES.blue, DICES.yellow],
    range: RANGES.malee,
    trait: ITEMS_TRAITS.hammer,
    worn:	WORN.oneHand,
  },
  
}

export const SEARCH_ITEMS = {
  curseDoll: {
    name: 'Curse Doll',
    gold: 50,
    amount: 1,
  },
  fireFlask: {
    name: 'Fire Flask',
    gold: 50,
    amount: 1,
  },
  healthPotion: {
    name: 'Health Potion',
    gold: 25,
    amount: 3,
  },
  nothing: {
    name: 'Nothing',
    gold: 0,
    amount: 1,
  },
  powerPotion: {
    name: 'Power Potion',
    gold: 50,
    amount: 1,
  },
  staminaPotion: {
    name: 'Stamina Potion',
    gold: 25,
    amount: 3,
  },
  treasureChest: {
    name: 'Treasure Chest',
    gold: 0,
    amount: 1,
  },
  wardingTalisman: {
    name: 'Warding Talisman',
    gold: 50,
    amount: 1,
  },
}


export class Item {
  constructor(desc) {
    Object.assign(this, desc);
  }
}